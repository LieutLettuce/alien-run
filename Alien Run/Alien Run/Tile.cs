﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Alien_Run
{
    class Tile
    {
        // -------------------------------------
        // Data
        // -------------------------------------

        private Texture2D sprite;
        private Vector2 position;


        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Tile(Texture2D newSprite, Vector2 newPosition)
        {
            sprite = newSprite;
            position = newPosition;
        }
        // -------------------------------------

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        // -------------------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }
        // -------------------------------------
    }
}
