﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;

namespace Alien_Run
{
    class Player
    { 
        // -------------------------------------
        // Data
        // -------------------------------------
        private Vector2 position = Vector2.Zero;
        private Vector2 velocity = Vector2.Zero;
        private Texture2D sprite = null;
        private Level ourLevel = null;


        // Constants

        private const float MOVE_SPEED = 300.0f;
        private const float GRAVITY_ACCEL = 3400f;
        private const float TERMINAL_VEL = 550.0f;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("player/player-stand");
        }
        // -------------------------------------
        public void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //update our velocity based on input and gravity, etc
            // Horizontal v is always constant because this is an auto run game.
            velocity.X = MOVE_SPEED;

            //apply acceleration due to gravity
            velocity.Y += GRAVITY_ACCEL * deltaTime;

            //Clamp our velocity to our terminal range
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VEL, TERMINAL_VEL);

            //pos2 = pos1 + deltaPos
            //deltaPos = velocity * deltaTime
            position += velocity * deltaTime;

            // Check if we are colliding with anything
            CheckTileCollision();
        }
        // -------------------------------------
        private void CheckTileCollision()
        {
            // Get the player's bounding box to get a list of tileswe are colliding with.
            Rectangle playerBounds = GetBounds();
            // How to get list of Tiles? -- Ask Level
            List<Tile> collidingTiles = ourLevel.GetTilesInBounds(playerBounds);

            // For each colliding tile, move ourselves out of the tile

            foreach (Tile collidingTile in collidingTiles)
            {
                // Determin how ar we are overlapping other tiles.
                Rectangle tileBounds = collidingTile.GetBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, playerBounds);


                //Only resolve collision if there actually was one
                // Depth will be Vector2.Zero if there was nno collision
                if (depth != Vector2.Zero)
                {
                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);

                    // Resolvbe the collision along the shallow axis, as that is the one 
                    // we are closest  to the edge of and therefor easier to "squeeze out"
                    // or you can think of it as we only just overlapped on that side

                if (absDepthY < absDepthX)
                    {
                        // Y is our shallow axis
                        // Resolve the collision along the Y axis
                        position.Y += depth.Y;

                        playerBounds = GetBounds();
                    }
                else
                    {
                        // X is our shallow axis
                        // Resolve the collision along the X axis
                        position.X += depth.X;

                        //Recalculate bounds for the future collision checking
                        playerBounds = GetBounds();
                        
                    }
                }

            }

        }
        // -------------------------------------
        private Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }
        // -------------------------------------
        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            // This function calculates how far our rectangles are overlapping

            // Calculate the half sizes of both rectangles
            float halfWidthPlayer = player.Width / 2.0f;
            float halfHeightPlayer = player.Width / 2.0f;

            float halfWidthTile = tile.Width / 2.0f;
            float halfHeightTile = tile.Width / 2.0f;

            // Calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(player.Left + halfWidthPlayer, player.Top + halfHeightPlayer);
            Vector2 centreTile = new Vector2(tile.Left + halfWidthTile, tile.Top + halfHeightTile);

            // How far away are the centres of each of these rectangles from each other

            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            // Minimum distance these need to be to NOT collide / intersect
            // IF EITHER THE X OR THE Y DISTANCE IS GREATER THAN THESE MINMA, THESE ARE NOT INTERSECTING

            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfHeightTile;

            //if we are not intersecting at all, return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // Calculate and return the intersection depth
            //Essentially, how much ofver the minimum intersection are we in each direction
            // AKA by how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);

        }
        // -------------------------------------

    }
}
