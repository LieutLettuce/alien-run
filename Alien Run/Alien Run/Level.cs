﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;


namespace Alien_Run
{
    class Level
    {
        // -------------------------------------
        // Data
        // -------------------------------------   
        Tile[,] tiles;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;
        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGHT = 70;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            //Creating a single copy of the tile texture that will be used by all the tiles.
            Texture2D tileTexture = content.Load<Texture2D>("tiles/box");

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            CreateBox(0, 4, tileTexture);
            CreateBox(1, 4, tileTexture);
            CreateBox(2, 4, tileTexture);
            CreateBox(3, 4, tileTexture);
            CreateBox(4, 4, tileTexture);

        }
        // -------------------------------------
        public void CreateBox(int tileX, int tileY, Texture2D tileTexture)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(tileTexture, tilePosition);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x,y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        // -------------------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill the tiles
            List<Tile> tilesInBounds = new List<Tile>();

            // Determin the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGHT) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y < bottomTile; ++y)
                {
                    if (tiles[x, y] != null)
                        tilesInBounds.Add(tiles[x,y]);
                }
            }
            return tilesInBounds;
        }
        // -------------------------------------
    }
}
